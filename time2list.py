# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 18:26:29 2023

@author: Chris
"""

import pandas as pd
import numpy as np 

fps=30.

path=r"C:\Users\wiesbrock\Desktop\Repos\deepethogram_analysis\2022-13-10_urine_mouse415_side view_labels.csv"
data=pd.read_csv(path)

right_sniff=data['sniffingrightdish']
right_sniff=np.array(right_sniff)
right_sniff=np.diff(right_sniff)
start_right_sniff=np.array(np.where(right_sniff==1))
start_right_sniff=np.reshape(start_right_sniff,(np.shape(start_right_sniff)[1],1))
stop_right_sniff=np.array(np.where(right_sniff==-1))
stop_right_sniff=np.reshape(stop_right_sniff,(np.shape(stop_right_sniff)[1],1))

seconds_start_right_sniff=start_right_sniff/fps
seconds_stop_right_sniff=stop_right_sniff/fps


start_right_list = []  
stop_right_list = []

for s in seconds_start_right_sniff:
    minutes = s // 60 
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000)  


    start_right_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))
    
for s in seconds_stop_right_sniff:
    minutes = s // 60  
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000) 


    stop_right_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))
    
left_sniff=data['sniffingleftdish']
left_sniff=np.array(left_sniff)
left_sniff=np.diff(left_sniff)
start_left_sniff=np.array(np.where(left_sniff==1))
start_left_sniff=np.reshape(start_left_sniff,(np.shape(start_left_sniff)[1],1))
stop_left_sniff=np.array(np.where(left_sniff==-1))
stop_left_sniff=np.reshape(stop_left_sniff,(np.shape(stop_left_sniff)[1],1))

seconds_start_left_sniff=start_left_sniff/fps
seconds_stop_left_sniff=stop_left_sniff/fps


start_left_list = []  
stop_left_list = []

for s in seconds_start_left_sniff:
    minutes = s // 60  
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000)  


    start_left_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))
    
for s in seconds_stop_left_sniff:
    minutes = s // 60  
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000)  


    stop_left_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))


list1 = start_left_list
list2 = stop_left_list


with open("times_left.txt", "w") as file:


    for i in range(len(list1)):
        line = f"{list1[i]} - {list2[i]}\n"  
        file.write(line)  
        

list1 = start_right_list
list2 = stop_right_list


with open("times_right.txt", "w") as file:

   
    for i in range(len(list1)):
        line = f"{list1[i]} - {list2[i]}\n" 
        file.write(line)  
        
diff_left=seconds_stop_left_sniff-seconds_start_left_sniff
diff_right=seconds_stop_right_sniff-seconds_start_right_sniff

diff_left_list=[]
for s in diff_left:
    minutes = s // 60  
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000)  


    diff_left_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))
    
diff_right_list=[]
for s in diff_right:
    minutes = s // 60  
    seconds_remainder = s % 60  
    milliseconds = np.round((s - int(s)) * 1000)  


    diff_right_list.append('{:02d}:{:02d}:{:03d}'.format(int(minutes), int(seconds_remainder), int(milliseconds)))

list1 = diff_left_list

with open("period_left.txt", "w") as file:

   
    for i in range(len(list1)):
        line = f"{list1[i]}\n" 
        file.write(line)  
        
list1 = diff_right_list

with open("period_right.txt", "w") as file:

   
    for i in range(len(list1)):
        line = f"{list1[i]}\n" 
        file.write(line)  


